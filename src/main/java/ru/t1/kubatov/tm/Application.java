package ru.t1.kubatov.tm;

import ru.t1.kubatov.tm.constant.ArgumentConst;
import ru.t1.kubatov.tm.constant.TeminalConst;
import java.util.Scanner;

public class Application {

    public static void main(final String[] args) {
        parseArguments(args);
        parseCommands();
    }

    private static void parseArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArgument(arg);
    }

    private static void parseCommands() {
        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while(!Thread.currentThread().isInterrupted()){
            System.out.println("ENTER COMMAND:");
            final String command = scanner.next();
            parseCommand(command);
        }
    }

    private static void parseCommand(final String command) {
        if(command == null || command.isEmpty()) return;
        switch (command) {
            case TeminalConst.VERSION:
                showVersion();
                break;
            case TeminalConst.HELP:
                showHelp();
                break;
            case TeminalConst.INFO:
                showDeveloperInfo();
                break;
            case TeminalConst.EXIT:
                exit();
                break;
            default:
                showCommandError();
        }
    }

    private static void showCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This command is not supported...");
    }

    private static void parseArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.INFO:
                showDeveloperInfo();
                break;
            default:
                showArgumentError();
        }
        System.exit(0);
    }

    private static void showArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported");
        System.exit(1);
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showWelcome() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s - Show application version. \n", TeminalConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s - Show application commands. \n", TeminalConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s, %s - Show developer info. \n", TeminalConst.INFO, ArgumentConst.INFO);
        System.out.printf("%s - Close application. \n", TeminalConst.EXIT);
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.4.0");
    }

    private static void showDeveloperInfo() {
        System.out.println("[DEVELOPER]");
        System.out.println("NAME: Shamil Kubatov");
        System.out.println("E-MAIL: shamil.kubatov@mail.ru");
    }

}